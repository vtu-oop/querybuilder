namespace QueryBuilder;

public interface IQueryBuilder<T>
{
    string GetAll();
    string Get(int id);
    string Insert(T entity);
    string Update(T entity);
    string Delete(T entity);
}