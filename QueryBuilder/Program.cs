﻿// See https://aka.ms/new-console-template for more information

using System.Security.AccessControl;
using System.Threading.Channels;
using QueryBuilder;

QueryBuilder<User> qb = new QueryBuilder<User>();
Console.WriteLine(qb.GetAll());
Console.WriteLine(qb.Get(5));

User user = new User();
user.Id = 34;
user.Email = "john@example.com";
user.FullName = "John Smith";
user.Password = "P@ssword1";

Console.WriteLine(qb.Insert(user));

QueryBuilder<Product> pqb = new QueryBuilder<Product>();
Console.WriteLine(pqb.Get(34));

Product product = new Product();
product.Id =21343;
product.Brand = "SomeBrand";
product.Name = "AnyName";
product.Price = 234.34m;

Console.WriteLine(pqb.Insert(product));