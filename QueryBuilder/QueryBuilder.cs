using System.Reflection;

namespace QueryBuilder;

public class QueryBuilder<T> : IQueryBuilder<T>
{
    public string GetAll()
    {
        return $"SELECT * FROM {typeof(T).Name}";
    }

    public string Get(int id)
    {
        return $"SELECT * FROM {typeof(T).Name} WHERE Id = {id}";
    }

    public string Insert(T entity)
    {
        string tableName = typeof(T).Name;
        string columnsNames = GetPropertiesNames();
        string columnsValues = GetPropertiesValues(entity);
        
        return $@"INSERT INTO {tableName} ({columnsNames})
                  VALUES ({columnsValues})";
    }

    private string GetPropertiesNames()
    {
        List<string> names = new List<string>();
        foreach (var pi in typeof(T).GetProperties())
        {
            names.Add(pi.Name);
        }
        
        return string.Join(",", names);
    }

    private string GetPropertiesValues(T obj)
    {
        List<string> values = new List<string>();
        foreach (var pi in typeof(T).GetProperties())
        {
            values.Add(pi.GetValue(obj).ToString());
        }

        return string.Join(",", values);
    }
    
    public string Update(T entity)
    {
        throw new NotImplementedException();
    }

    public string Delete(T entity)
    {
        throw new NotImplementedException();
    }
}